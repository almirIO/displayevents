<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WDV341 Intro PHP  - Display Events Example</title>
    <style>
		body {
			width:800px;
			margin:auto;
			padding:10px;
		}
		td {
			text-align:center;
			padding:10px;
		}
		table {
			margin:auto;
		}
		label {
			font-size:18px;
		    font-weight: bold;
		    font-family:Arial;
		}
		h2 {
			color:red;
			text-align:center;
		}
		th {
			color:red;
			font-size:20px;
		    font-family: Arial;
		}
</style>
		
	</style>
</head>

<body>
    <h1>WDV341 Intro PHP</h1>
    <h3> Events are available today.</h3>
	<table>
	<tablehead>
		<tr>
			<th>Number</th>
			<th>Event</th>
			<th>Description</th>
			<th>Day</th>
			<th>Time</th>
		</tr>
	</tablehead>

<?php
	require('connection.php'); //requires the PDO connection file
	$results = $conn->prepare("SELECT * FROM wdv341_events ORDER BY event_id"); //prepares statement that selects from  the database's table and orders the items by the event_id
	$results->execute(); //executes the prepared statement
	for($i=0; $row = $results->fetch(); $i++){ //for loop function that fetches the data and increments each column starting with event_id until there's none left
		?>

		<tr>
			<td><label><?php echo $row['event_id']; ?></label></td> <!-- echos out the event_id -->
			<td><label><?php echo $row['event_name']; ?></label></td><!-- echos out the event_names -->
			<td><label><?php echo $row['event_description']; ?></label></td><!--echos out the event_description -->
			<td><label><?php echo $row['event_day']; ?></label></td><!-- echos out the event_day-->
			<td><label><?php echo $row['event_time']; ?></label></td><!--echos out the event_time -->
		</tr>

		
		<?php } //this closes the for loop?>


</table>
</div>	
</body>
</html>